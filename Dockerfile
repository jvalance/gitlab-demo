FROM node:8
RUN mkdir /app
WORKDIR /app
COPY . .
RUN npm install

USER node:node

EXPOSE 22

ENTRYPOINT ["node", "/app/index.js"]
